package com.will.mapreduce.wordcount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class WordCountMapper extends Mapper<LongWritable,Text,Text, IntWritable> {
    //为了节省空间，将k-v设置到函数外
    private Text outK=new Text();
    private IntWritable outV=new IntWritable(1);


    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, IntWritable>.Context context) throws IOException, InterruptedException {
        //获取一行输入数据
        String line = value.toString();
        //将数据切分
        String[] words = line.split(" ");
        //循环每个单词进行k-v输出
        for (String word : words) {
            outK.set(word);
            //将参数传递到reduce
            context.write(outK,outV);
        }
    }
}
