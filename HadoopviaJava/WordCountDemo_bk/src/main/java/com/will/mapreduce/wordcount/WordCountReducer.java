package com.will.mapreduce.wordcount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class WordCountReducer extends Reducer<Text, IntWritable,Text,IntWritable> {
    //全局变量输出类型
    private IntWritable outV = new IntWritable();
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values,Context context) throws IOException, InterruptedException {		//设立一个计数器
        int sum=0;
        //统计单词出现个数
        for (IntWritable value : values) {
            sum+=value.get();
        }
        //转换结果类型
        outV.set(sum);
        //输出结果
        context.write(key,outV);
    }
}

