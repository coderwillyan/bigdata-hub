package org.example;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class RemoteHDFSFileUpload {
    public void doUpload() {
        // 1. 设置Hadoop配置
        Configuration conf = new Configuration();
        // 这个解决hdfs问题
        conf.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        // 这个解决本地file问题
        conf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());


        // 2. 指定HDFS的URI，包括HDFS主机和端口
        URI hdfsUri = URI.create("hdfs://192.168.0.191:8020");

        // 3. 获取HDFS文件系统对象
        FileSystem fs = null;
        try {
            fs = FileSystem.get(hdfsUri, conf);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // 4. 指定本地文件和目标HDFS路径
        String localFilePath = "H://academic-hub//bigdata-hub//BigData-mds//data//employee_doupload.txt";
        String hdfsFilePath = "/case/hdfscase/"; // 修改为目标HDFS路径

        try {

            fs.copyFromLocalFile(new Path(localFilePath), new Path(hdfsFilePath));
            fs.close();

            System.out.println("文件上传成功！");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 9. 关闭HDFS文件系统连接
            try {
                fs.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

    }
}