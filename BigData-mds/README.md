# BigData-Notes



**大数据常用组件指南**



<table>
    <tr>
      <th><img width="50px" src="images/hadoop.jpg"></th>
      <th><img width="50px" src="images/hbase.png"></th>
      <th><img width="50px" src="images/hive.jpg"></th>
      <th><img width="50px" src="images/spark.jpg"></th>
      <th><img width="50px" src="images/storm.png"></th>
      <th><img width="50px" src="images/flink.png"></th>
      <th><img width="50px" src="images/sqoop.png"></th>
      <th><img width="50px" src="images/flume.png"></th>
      <th><img width="50px" src="images/kafka.png"></th>
      <th><img width="50px" src="images/zookeeper.jpg"></th>
    </tr>
    <tr>
      <td align="center"><a href="#一hadoop">Hadoop</a></td>
      <td align="center"><a href="#二hbase">HBase</a></td>
      <td align="center"><a href="#三hive">Hive</a></td>
      <td align="center"><a href="#四spark">Spark</a></td>
      <td align="center"><a href="#五storm">Storm</a></td>
      <td align="center"><a href="#六flink">Flink</a></td>
      <td align="center"><a href="#七sqoop">Sqoop</a></td>
      <td align="center"><a href="#八flume">Flume</a></td>
      <td align="center"><a href="#九kafka">Kafka</a></td>
      <td align="center"><a href="#十zookeeper">Zookeeper</a></td>
    </tr>
  </table>






## :black_nib: 前  言

1. [大数据综述](0.0大数据综述.md)
2. [大数据组件演变](0.1大数据组件演变史.md) 
3. [大数据学习线路](0.2大数据学习路线.md)
4. [大数据学习资料分享](0.3大数据学习资料分享.md)

## 一、Hadoop

1. [分布式文件存储系统 —— HDFS理论](1.1.1HDFS理论.md)
2. [分布式文件存储系统 —— HDFS实操](1.1.2HDFS实操.md)
3. [分布式计算框架 —— MapReduce理论](1.2.1MapReduce理论.md)
4. [分布式计算框架 —— MapReduce实操](1.2.2MapReduce实操.md)
5. [集群资源管理器 —— YARN](1.3YARN理论.md)

## 二、HBase

1. [Hbase 理论](2.1HBase理论.md)
1. [Hbase 实操](2.2HBase实操.md)

## 三、Hive

1. [Hive理论](3.1Hive理论.md)
2. [Hive实操](3.2Hive实操.md)

## 四、Spark

**Spark Core :**

1. [Spark 理论](4.1.1Spark理论.md)
1. [Spark 实操SparkSubmit](4.1.2Spark实操-SparkSubmit.md)
1. [Spark 实操SparkShell](4.1.3Spark实操-SparkShell.md)

**Spark SQL :**

1. [SparkSQL理论](4.2.1SparkSQL理论.md)
1. [SparkSQL实操](4.2.2SparkSQL实操.md)

**Spark Streaming ：**

1. [Spark Streaming 理论](4.3SparkStreaming.md)
1. [Spark Streaming + Flume + Kafka实操](10.Flume&Kafka&SparkStraming实操.md)



## 五、Storm

1. [Storm理论](5.1流处理框架Storm)



## 六、Flink

1. [Flink理论](5.2流处理框架Flink.md)

   

## 七、Sqoop

1. [Sqoop理论](7.1Sqoop理论.md)
1. [Sqoop实操](7.2Sqoop实操.md)



## 八、Flume

1. [Flume理论](7.2Sqoop实操)

   

## 九、Kafka

1. [Kafka 理论](9.1Kafka理论.md)

   

## 十、Zookeeper

1. [Zookeeper理论](6.1ZooKeeper理论.md)
1. [Zookeeper实操](6.2Zookeeper实操.md)

<div align="center">
