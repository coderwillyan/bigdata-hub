//package com.heibaiying.kafka
//
////import org.apache.kafka.common.serialization.StringDeserializer
////import org.apache.spark.SparkConf
////import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
////import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
////import org.apache.spark.streaming.kafka010._
////import org.apache.spark.streaming.{Seconds, StreamingContext}
//
//
//import org.apache.spark._
//import org.apache.spark.SparkConf
//import org.apache.spark.streaming._
//import org.apache.spark.streaming.kafka010
//import org.apache.spark.streaming.StreamingContext._
//import org.apache.spark.streaming.kafka010.KafkaUtils
//
//
///**
//  * spark streaming 整合 kafka
//  */
//object KafkaWordCounter {
//
//  def main(args: Array[String]) {
//    StreamingExamples.setStreamingLogLevels()
//    val sc = new SparkConf().setAppName("KafkaWordCounter").setMaster("local[2]")
//    val ssc = new StreamingContext(sc, Seconds(10))
//    ssc.checkpoint("file:///home") //设置检查点
//    val zkQuorum = "192.168.0.191:2181" //Zookeeper服务器地址
//    val group = "1" //topic所在的group，可以设置为自己想要的名称，比如不用1，而是val group = "test-consumer-group"
//    val topics = "flume-kafka" //topics的名称
//    val numThreads = 1 //每个topic的分区数
//    val topicMap = topics.split(",").map((_, numThreads.toInt)).toMap
//    val lineMap = KafkaUtils.createStream(ssc, zkQuorum, group, topicMap)
//    val lines = lineMap.map(_._2)
//    val words = lines.flatMap(_.split(" "))
//    val pair = words.map(x => (x, 1))
//    val wordCounts = pair.reduceByKeyAndWindow(_ + _, _ - _, Minutes(2), Seconds(10), 2)
//    wordCounts.print
//    ssc.start
//    ssc.awaitTermination
//  }
//
////  def main(args: Array[String]): Unit = {
////
////    val sparkConf = new SparkConf().setAppName("KafkaDirectStream").setMaster("local[2]")
////    val streamingContext = new StreamingContext(sparkConf, Seconds(5))
////
////    val kafkaParams = Map[String, Object](
////      /*
////       * 指定broker的地址清单，清单里不需要包含所有的broker地址，生产者会从给定的broker里查找其他broker的信息。
////       * 不过建议至少提供两个broker的信息作为容错。
////       */
////      "bootstrap.servers" -> "hadoop001:9092",
////      /*键的序列化器*/
////      "key.deserializer" -> classOf[StringDeserializer],
////      /*值的序列化器*/
////      "value.deserializer" -> classOf[StringDeserializer],
////      /*消费者所在分组的ID*/
////      "group.id" -> "spark-streaming-group",
////      /*
////       * 该属性指定了消费者在读取一个没有偏移量的分区或者偏移量无效的情况下该作何处理:
////       * latest: 在偏移量无效的情况下，消费者将从最新的记录开始读取数据（在消费者启动之后生成的记录）
////       * earliest: 在偏移量无效的情况下，消费者将从起始位置读取分区的记录
////       */
////      "auto.offset.reset" -> "latest",
////      /*是否自动提交*/
////      "enable.auto.commit" -> (true: java.lang.Boolean)
////    )
////
////    val topics = Array("spark-streaming-topic")
////    val stream = KafkaUtils.createDirectStream[String, String](
////      streamingContext,
////      PreferConsistent,
////      Subscribe[String, String](topics, kafkaParams)
////    )
////
////    /*打印输入流*/
////    stream.map(record => (record.key, record.value)).print()
////
////    streamingContext.start()
////    streamingContext.awaitTermination()
////  }
//
//}

import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord}
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Duration, Seconds, StreamingContext}

object SSKafka_Direct {
  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setMaster("local[*]").setAppName("streaming")
    val ssc = new StreamingContext(sparkConf,Duration(3000))  //采集周期10s
    //TODO SparkStreaming读取Kafka的数据
    //kafka配置信息
    val kafkaPara: Map[String, Object] = Map[String, Object](
      //zookeeper地址
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> "192.168.0.191:6667",
      ConsumerConfig.GROUP_ID_CONFIG -> "app",
      "key.deserializer" -> "org.apache.kafka.common.serialization.StringDeserializer",
      "value.deserializer" -> "org.apache.kafka.common.serialization.StringDeserializer"
    )

    val kafkaDStream: InputDStream[ConsumerRecord[String, String]] =
      KafkaUtils.createDirectStream[String, String](
        ssc,
        LocationStrategies.PreferConsistent,
        //订阅的topic名kafka_spark
        ConsumerStrategies.Subscribe[String, String](Set("flume-kafka1"), kafkaPara))
    val valueDStream: DStream[String] = kafkaDStream.map(record=>record.value())
    valueDStream.flatMap(_.split(" ")).map((_,1)).reduceByKey(_+_).print()

    ssc.start()
    ssc.awaitTermination()
  }
}

