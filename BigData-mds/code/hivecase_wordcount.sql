-- 创建words表
create external table docs(line string);

-- 查询表
select * from docs;

-- 加载wc.txt文件
load data inpath '/case/wordcount/input/wc.txt' into table docs;


-- 创建结果表
create table wc_result(
word string,
ct int
);

-- 查看数组结构
select explode(split(line,' ')) as word from docs;

-- 再次切割
from (select explode(split(line,' ')) as word from docs) t1
insert into wc_result
select t1.word,count(t1.word) group by t1.word;

-- 查询结果
select * from wc_result ;

